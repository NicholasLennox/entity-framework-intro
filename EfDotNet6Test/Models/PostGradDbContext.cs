﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfDotNet6Test.Models
{
    internal class PostGradDbContext : DbContext
    {
        public DbSet<Professor> Professors { get; set; } = null!;
        public DbSet<Student> Students { get; set; } = null!;
        public DbSet<Project> Projets { get; set; } = null!;
        public DbSet<Subject> Subjects { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source = ACC-NLENNOX\\SQLEXPRESS; Initial Catalog = PostGradEf; Integrated Security = True; Trust Server Certificate = True;");
        }
    }
}
