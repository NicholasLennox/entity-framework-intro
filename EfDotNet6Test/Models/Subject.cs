﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfDotNet6Test.Models
{
    [Table("Subject")]
    internal class Subject
    {
        public int Id { get; set; }
        public string SubCode { get; set; } = null!; // null! removes the weird warning. It happens with the generation as well.
        public string SubTitle { get; set; } = null!;

        // Relationships
        public int? ProfessorId { get; set; }
        public Professor? Professor { get; set; }
        public ICollection<Student>? Students { get; set; }
    }
}
