﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfDotNet6Test.Models
{
    [Table("Professor")]
    internal class Professor
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;
        public string? Field { get; set; }

        // Navigation properties
        public ICollection<Student>? Students { get; set; }
        public ICollection<Subject>? Subjects { get; set; }
    }
}
