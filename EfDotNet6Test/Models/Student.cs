﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EfDotNet6Test.Models
{
    [Table("Student")]
    internal class Student
    {
        public int Id { get; set; }
        public string Name { get; set; } = null!;

        // Relationships
        public int? ProfessorId { get; set; }
        // Navigation property
        public Professor? Professor { get; set; }
        public Project? Project { get; set; }
        public ICollection<Subject>? Subjects { get; set; }
    }
}
